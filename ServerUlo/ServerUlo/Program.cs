﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

namespace ServerUlo
{
    class Server
    {
        const string IP_ADDRESS = "127.0.0.1";
        const int PORT = 5000;
        private static Dictionary<int, User> UserList = new Dictionary<int, User>();
        private static Dictionary<int, TcpClient> ClientList = new Dictionary<int, TcpClient>();
        private static IFormatter formatter = new BinaryFormatter();
        private static int clientCounter;
        private static string[] User = { "User1", "User2", "User3" };
        private static string[] Pass = { "Pass3", "Pass2", "Pass3" };


        private static void UloServer(object argument)
        {
            int ID = clientCounter;
            TcpClient client = (TcpClient)argument;
            NetworkStream networkStream = client.GetStream();
            ClientList.Add(ID, client);
            ConnectionHandler(client);

            try
            {
                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                while (true)
                {
                    string fromClient = reader.ReadLine();
                    Console.WriteLine(fromClient);
                    writer.WriteLine(fromClient);
                    writer.Flush();
                }
            }
            catch (IOException)
            {
                Console.WriteLine("Client Disconnected");
            }
            if (client != null)
            {
                client.Close();
            }
        }
        public static bool ValidateUserData(User userData)
        {
            if (userData != null)
            {
                for (int i = 0; i < User.Length; i++)
                {
                    if (userData.user == User[i] && userData.pass == Pass[i])
                    {
                        UserList.Add(clientCounter, userData);
                        return true;
                    }
                }
                return false;
            }
            else
            {
                Console.WriteLine("Account doesn't exist");
                return false;
            }
        }

        public static void TcpBroadCast(string data)
        {
            foreach (KeyValuePair<int, TcpClient> client in ClientList)
            {
                StreamWriter sw = new StreamWriter(client.Value.GetStream());
                sw.WriteLine(data);
                sw.Flush();
            }
        }

        static void ConnectionHandler(TcpClient client)
        {
            StreamWriter writer = new StreamWriter(client.GetStream());
            NetworkStream networkStream = client.GetStream();

            formatter.Binder = new CustomizedBinder();
            User user = (User)formatter.Deserialize(networkStream);

            if (ValidateUserData(user))
            {
                Console.WriteLine("User Logged In : " + user.user);
                TcpBroadCast("User Logged In : " + user.user);
            }

            else
            {
                Console.WriteLine("Connection Error, Account Doesn't Exist");
            }
        }

        public static void Main()
        {
            TcpListener listener = null;
            clientCounter = 0;
            try
            {
                listener = new TcpListener(IPAddress.Parse(IP_ADDRESS), PORT);
                listener.Start();

                Console.WriteLine("Server Start");

                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine("Client Connected");
                    clientCounter++;
                    Thread newThread = new Thread(UloServer);

                    newThread.Start(client);
                }
            }

            catch (Exception a)
            {
                Console.WriteLine(a);
            }

            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }

    }

}
