﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace ServerUlo
{
    sealed class CustomizedBinder : SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type returntype = null;
            string sharedAssemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            assemblyName = Assembly.GetExecutingAssembly().FullName;
            typeName = typeName.Replace(sharedAssemblyName, assemblyName);
            returntype = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));

            return returntype;
        }

        public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            base.BindToName(serializedType, out assemblyName, out typeName);
            assemblyName = "SharedAssembly, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
        }
    }


    public static class ByteArraySerializer
    {
        // from obj to byte
        public static byte[] Serializer<T>(this T serializer)
        {
            using (var ms = new MemoryStream())
            {
                new BinaryFormatter().Serialize(ms, serializer);
                return ms.ToArray();
            }
        }

        // from byte to object
        public static T Deserializer<T>(this byte[] deserializer)
        {
            using (var ms = new MemoryStream(deserializer))
            {
                return (T)new BinaryFormatter().Deserialize(ms);
            }
        }
    }
}
