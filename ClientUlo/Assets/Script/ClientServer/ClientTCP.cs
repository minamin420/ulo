﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using UnityEngine.UI;
public class ClientTCP : MonoBehaviour
{
    public Text textFromServer;
    public InputField textToServer;

    private static string Adress = "127.0.0.1";
    private static int Port = 5000;
    private StreamWriter sw;
    private StreamReader sr;

    TcpClient client = new TcpClient(Adress, Port);
    // Start is called before the first frame update
    void Start()
    {
        try
        {
            sr = new StreamReader(client.GetStream());
            sw = new StreamWriter(client.GetStream());
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    public void SendData()
    {
        sw.WriteLine(textToServer.text);
        sw.Flush();
        String servString = sr.ReadLine();
        textFromServer.text = servString;
    }

    private void OnDisable()
    {
        sr.Close();
        sw.Close();
        client.Close();
    }
}
