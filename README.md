# ULO

![](https://gitlab.com/minamin420/ulo/-/raw/master/Pictures/menu.png)

## Game Overview
**Description** <br>
In this game, the player will become a dragon character where this character only has 2 goals, namely eating food scattered throughout the map and avoiding collisions by other players. The concept of this game is almost similar to Tron, where to win the game you must destroy the enemy by blocking the path. But in this game, to be able to block the opponent's path, you have to eat the food scattered on the map to lengthen his body.

## Features
- Free for Everyone
- Online Multiplayer 

## Game Information
- Platform   :  Desktop (Windows) <br>
- Rating   : Everyone 10+ <br>
- Genre : Casual, Arcade

## Project Information
- Network Protocol : TCP and UDP <br>
- Game Engine : Unity 2019.4.15f 

## Gameplay
![](https://gitlab.com/minamin420/ulo/-/raw/master/Pictures/gameplay_ulo.gif)

## Game Flowchart
![](https://gitlab.com/minamin420/ulo/-/raw/master/Pictures/dmgo.png)

## Packets Used

```
public enum ServerPackets
{
    tcpTest = 1,
    udpTest,
    login,
    signUp,
    playerConnect,
    playerDisconnected,
    spawnFood,
    destroyFood,
    playerSpawn,
    playerPosition,
    playerRotation
}

public enum ClientPackets
{
    tcpTestReceived = 1,
    loginInput,
    signUpInput,
    playerMovement,
    playerEat
}

```

## Authored by
- 4210181015 - [Muhammad Farhan Irfani](https://gitlab.com/farhanirfani)
- 4210181027 - [Muhammad Amien Prananto](https://gitlab.com/minamin420)
